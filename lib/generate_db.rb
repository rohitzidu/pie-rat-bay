root = File.expand_path('../../', __FILE__)

require 'data_mapper'
require root + '/models/torrent.rb'

src_file = "#{root}/src/complete"
split_regex = /^([0-9]+)\|(.+)\|([0-9]+)\|([0-9]+)\|([0-9]+)\|([0-9a-fA-F]{38,40})$/

DataMapper.setup(:default, ENV['DATABASE_URL'] || "sqlite:data")
DataMapper.finalize
DataMapper.auto_upgrade!

File.read(src_file).split("\n").each do |line|
  id, title, size, seeders, leechers, magnet = line.scan(split_regex)[0]
  puts title
  Torrent.create!(:id => id, :title => title, :size => size, :seeders => seeders, :leechers => leechers, :magnet => magnet)
end

puts "Finished!!!"